TB Plane

A two button endless flyer game made originaly for Godot Engine Community Jam 12/16-01/17

This branch is for Godot 4.0, imported from 3.5 which was updated from the
3.0 version also imported from 2.1 
Older version are located on their respective branches.

Press up or down to start and to control the plane, avoid hiting anything.

Assets credits:
- Most graphics assets are from Kenney (https://kenney.nl)
- Explosion by master484 (http://opengameart.org/users/master484)
- Crash sound by qubodup (http://opengameart.org/users/qubodup)
All CC0

Plane sounds I'm sure were from freesounds, lost sources, will keep looking, 
don't reuse them, just in case...

Extra credits:

Puppetmaster (https://puppetmaster.itch.io/ https://twitter.com/fischspiele / https://github.com/puppetmaster-)

for his idea of using paths to move screens in ZEFRO.

+>e
