extends RigidBody2D

#The player
#I don't like to use rigid bodies for this kind of things but I did it anyway
#This controller has a simple way to manipulate a rigid body
#using the default integrator, some things were changed on the properties
#and the physics engine, there is 0 damp overriden by the body, some angular
#and zero general gravity

#I set the default moving speed here too
@onready var speed = linear_velocity
var ignore_rotation =true # reversed "rotating" for Camera2D
var alive = true

func _ready():
	linear_velocity = Vector2() #I set it and remove it, yeah
	$flysfx.play()
	

func _physics_process(_delta):
	if alive:
		var modifier = 1.0
		#This allows to test the scene without a main one
		if has_node("/root/game"):
			modifier = $"/root/game".speed_modifier
		
		#The body just rotates
		if Input.is_action_pressed("ui_up"):
			angular_velocity = 10*modifier
		if Input.is_action_pressed("ui_down"):
			angular_velocity = -10*modifier
		
		#Up to certain limits
		if angular_velocity!=0:
			if (rotation_degrees+180<=-80&& angular_velocity>0) || 				\
				(rotation_degrees-180>=80&& angular_velocity<0):			#   \ <- allow break lines
				angular_velocity = 0
			
			#Remember speed? is used to calculate the Y part of it according to the rotation
			var current_speed = transform.basis_xform(speed*modifier)
			#Because is locked on X
			current_speed.x = 0
			#So, we set the speed just on Y to simulate the movement
			linear_velocity = current_speed
		
		#If touch anything, dies
		#If you add items, can be filtered by groups here
		if (get_colliding_bodies().size()>0):
			$smokeparticles.emitting = false
			$deadparticles.emitting = true
			$crashsfx.play()
			alive = false
	
	#done
