extends Path2D

#The endless scroller

var started

func _physics_process(delta):
	var modifier = 1.0
	if has_node("/root/game"):
		modifier = get_node("/root/game").speed_modifier
	
	#just iterates over the PathFollow2D children and move them
	for c in get_children():
		(c as PathFollow2D).progress+=100*delta*modifier
	

func start():
	started = true
