extends Area2D

#All the magic is here

#is easy to append more presets from pck files here
var presets = [  "res://zonepresets/preset1.tscn",
						"res://zonepresets/preset2.tscn",
						"res://zonepresets/preset3.tscn",
						"res://zonepresets/preset4.tscn",
						"res://zonepresets/preset5.tscn",
						"res://zonepresets/preset6.tscn",
						"res://zonepresets/preset7.tscn",
						"res://zonepresets/preset8.tscn",
						"res://zonepresets/preset9.tscn",
						"res://zonepresets/preset10.tscn",
						]

#This clears and populates the zones/screens
#As one of the labels says, this could be a lot more complex
#Dividing the presets on layers and adding rules for combinations
#will result in a high variety of controlled screens with a 
#low rate of undesirable results (like impossible ones)
func recreate_zone():
	if has_node("preset"):
		var op = $"preset"
		op.name = "preset-old"
		op.queue_free()
	if presets.size()>0:
		var p = load(presets[randi()%presets.size()]).instantiate()
		add_child(p)
		p.name = "preset"

#Here is where the line area works after a path cycle
func _on_zone_area_enter( area ):
	if area.is_in_group("zonetrigger"):
		call_deferred("recreate_zone")
	
