extends Node2D

#the main script

var speed_modifier = 1.0
@export_node_path("RigidBody2D") var player_path = null 
var player = null

var started = false
var starting = false
var gameover = false

var distance = 0.0
var high_score = 0.0

func _ready():
	#load the score, the first thing to do
	load_highscore()
	set_physics_process(false)


func _physics_process(delta):
	#Control the initial status, no menu scene so all must be done with flags
	if (!started&&!starting):
		if Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down"):
			#Call group to start the gaming machine
			#This enables the important things on the game, like the progression timer and the zonetrigger 
			get_tree().call_group("gamegears","start")
			starting = true
	elif player.alive && started:
		#If the game reaches this line, it means that the game proper is running
		distance += 100.0*delta*speed_modifier

	if player.alive:
		#Yes, I can resume some ifs, anyway, here is your scrolling background
		#Check the texture and sprite options
		$ImageBackground/Sprite.region_rect.position += Vector2(50*speed_modifier*delta,0)
	else: 
		#player dead? game must end...
		if !gameover:
			#same as before, some preparations to reset the game and prevent instant reset
			get_tree().call_group("gameover","start")
			gameover = true
			#check the score
			if distance > high_score:
				high_score = distance
				save_highscore()
		elif !$inputreadytimer.time_left>0 && (Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down")):
			#reset the scene
			var _reset_status = get_tree().reload_current_scene()

#This is for progressiontimer
#Just reset the timer just in case I want a delay on the first one
#Is used to speed up the game slowly
func _on_Timer_timeout():
	if started:
		speed_modifier+=0.1
	elif starting:
		started = true
	if gameover:
		$"progressiontimer".stop()


#Autofire timer, used to allow input reading instead of other ways of input block
#Things normally done on _ready are delayed here too
func _on_gamereadytimer_timeout():
	if !gameover && has_node(player_path):
		player = get_node(player_path)
		randomize()
		set_physics_process(true)
	

#Simple save game
func save_highscore():
	var savegame = FileAccess.open("user://high.score", FileAccess.WRITE)
	savegame.store_float(floor(high_score))
	savegame.flush()
	

#Simple load game, 90% copypasta from the docs
func load_highscore():
	
	if !FileAccess.file_exists("user://high.score"):
		return 
	
	#var currentline = 0.0s
	var savegame = FileAccess.open("user://high.score", FileAccess.READ)
	high_score = savegame.get_real()
	
	savegame.flush()
	#high_score = currentline
	
	#this should be done by UI, not here...
	var hsg = get_tree().get_nodes_in_group("highscore")
	if hsg.size()>0: 
		hsg[0].set_text(str("BEST: " , str(floor(high_score),"m") if high_score<1000 else str(str(high_score/1000).pad_decimals(3),"km")) ) #new ternary operator
	
