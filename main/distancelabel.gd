extends Label

#This label just updates the distance to show
func _ready():
	set_process(false)

func _process(_delta):
	var dist = floor(get_node("/root/game").distance)
	text = str("Dist: " , str(dist,"m") if dist<1000 else str(str(dist/1000).pad_decimals(3),"km"))  #new ternary operator

func start():
	set_process(true)
